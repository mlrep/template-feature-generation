import pandas as pd
import numpy as np
from cvxpy import *

from mldev.experiment import experiment_tag


@experiment_tag()
class QPFS(object):
    def __init__(self, dataframe: pd.DataFrame, target: pd.DataFrame, target_value: str, id: pd.DataFrame,
                 index_id: str, feature_num):
        self.df = dataframe
        self.target_vector = target
        self.target_value = target_value
        self.index_id = index_id
        self.id = id
        self.Q = []
        self.b = []
        self.x = []
        self.feature_num = feature_num
        self.selected = pd.DataFrame
        self.vector_opt = []

    def _fit_vector(self):
        if len(self.target_vector.shape) == 1:
            self.vector_opt = self.target_vector[:, np.newaxis]
        else:
            self.vector_opt = self.target_vector[:]

    def _create_cor_matrix(self):
        temp_df = pd.DataFrame(np.hstack([self.df, self.vector_opt]))
        cor_matrix = np.array(temp_df.corr())
        self.Q = cor_matrix[:-1, :-1]
        assert np.all(self.Q == self.Q.T)
        self.b = cor_matrix[:-1, [-1]]

    def solve_qp(self):
        self._fit_vector()
        self._create_cor_matrix()
        n = self.Q.shape[0]
        self.x = Variable(n)
        constraints = [self.x >= 0, norm(self.x, 1) <= 1]
        formulation = Minimize(quad_form(self.x, self.Q) - self.b.T * self.x)
        problem = Problem(formulation, constraints)
        problem.solve()
        self.x = np.array(self.x.value).flatten()

    def selected_dataset(self):
        feature_subset = np.argsort(self.x)
        feature_subset = feature_subset[::-1][self.feature_num + 1:]
        to_drop = self.df.columns[feature_subset]
        selected = self.df
        selected.drop(to_drop, axis=1, inplace=True)
        return selected, self.target_vector,

    def write_data(self, path):
        feature_subset = np.argsort(self.x)
        feature_subset = feature_subset[::-1][self.feature_num + 1:]
        to_drop = self.df.columns[feature_subset]
        selected = self.df
        selected.drop(to_drop, axis=1, inplace=True)
        selected.to_csv(path)

